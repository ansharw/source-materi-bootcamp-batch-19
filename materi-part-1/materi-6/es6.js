// without let
var x = 1;

if (x == 1){
  var x = 3;
  console.log(x)
}

console.log(x)

// with let
let x = 1;

if (x == 1){
  let x = 3;
  console.log(x)
}

console.log(x)

const a = 2;
a = 3;

//arrow function example
const halo = () => {
  return "Halo Dunia";
}

const tambah = (angka1,angka2=2) => {
  return angka1+angka2;
}

console.log(halo())
console.log(tambah(4))

//without template literal
var firstNameExample = 'John'
var lastNameExample = 'Doe'
var teamNameExample = 'Mr'
 
var theStringExample = firstNameExample+ " " + lastNameExample +", " +teamNameExample
 
console.log(theStringExample) // John Doe, Mr

// with template literal

const firstName = 'John'
const lastName = 'Doe'
const teamName = 'Mr'
 
const theString = `${firstName} ${lastName}, ${teamName}`
 
console.log(theString) // John Doe, Mr

//without enhanched object literal

var nama = "John"
var tinggi = "170cm"
var berat = "60kg"

var orang = {
  nama: nama,
  tinggi: tinggi,
  berat: berat
}

console.log(orang)

// without enhanched object literal
let name = "John"
let height = "170cm"
let weight = "60kg"

let person = {
  name,
  height,
  weight
}

console.log(person)

//without destructuring

var angka = [1,2,3]

var angkaPertama = angka[0]
var angkaKedua = angka[1]
var angkaKetiga = angka[2]

console.log(angkaPertama)

// with destructuring

let numbers = [1,2,3]

let [numberOne, , numberThree] = numbers
console.log(numberThree)

let car = {name: "BMW", color: "black", year: 2020}

let {name: carName} = car

console.log(carName)

// Rest Parameters
 
//first example
let scores = ['98', '95', '93', '90', '87', '85']
let [first, second, third, ...restOfScores] = scores;
 
console.log(first) // 98
console.log(second) // 95
console.log(third) // 93
console.log(restOfScores) // [90, 87, 85] 

//second example 
const kalikan = (...rest) =>{
  const [angka1, angka2, angka3] = rest
  return angka1 * angka2 * angka3
}

console.log(kalikan(1, 2, 3))

// spread operator
let array1 = ['one', 'two']
let array2 = ['three', 'four']
let array3 = ['five', 'six']
 
// ES5 Way / Normal Javascript
 
var combinedArray = array1.concat(array2).concat(array3)
console.log(combinedArray) // ['one', 'two', 'three', 'four', 'five', 'six']
 
// ES6 Way 
 
let combinedArray = [...array1, ...array2, ...array3]
console.log(combinedArray) // ['one', 'two', 'three', 'four', 'five', 'six']

let arrAngka = [1,2,3]

let arrAngkaBaru = [4,5, ...arrAngka]

console.log(arrAngkaBaru)

let animal = {name: "Lion", food: "Meat"}

console.log(animal)
animal = {...animal, amountOfFoot: 4 }

console.log(animal)
